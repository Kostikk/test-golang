CONTAINERS = $(shell docker ps -q)

up:
	docker-compose up -d

upgrpcserver:
	cd servergrpc && go run main.go

upgrpcclient:
	cd client && go run main.go

build:
	docker build --tag test-task .

runttests:
	cd tests && go test -v

down:
	docker stop $(CONTAINERS)

delete:	down
	docker rm test
	docker rmi gotest_test_api
