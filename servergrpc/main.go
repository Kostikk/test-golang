package main

import (
	"context"
	"strconv"

	//"context"
	"github.com/joho/godotenv"
	"gotest/servergrpc/grpc"
	"log"
	"os"
	"os/signal"
	"syscall"
)

// main
func main() {
	_, cancel := context.WithCancel(context.Background())
	env, err := godotenv.Read(".env")
	if err != nil {
		log.Fatalf("Error loading .env file: %s", err)
	}

	port := string(env["PORTGRPC"])
	portint, _ := strconv.Atoi(port)
	log.Printf("--------listen grpc on addres - %s port - %d", string(env["SERVER_ADDRESS"]), portint)

	gs := grpc.NewGrpcServer()
	go gs.Run(string(env["SERVER_ADDRESS"]), portint)

	if err != nil {
		log.Fatalf("server error %s", err)
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit
	cancel()

	log.Print("<===exiting app===>")

	log.Print("server stopped")

}
