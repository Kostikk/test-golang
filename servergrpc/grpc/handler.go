package grpc

import (
	"context"
	"log"

	serv "gotest/servergrpc/pb"
)

// GetQuote get quote
func (s *ServerData) GetQuote(ctx context.Context, in *serv.GetQuoteRequest) (*serv.GetQuoteResponse, error) {
	s.RLock()
	var err error
	s.author, s.quote, err = s.RandomquoteGrpc()
	if err != nil {
		log.Fatalf(err.Error())
		return nil, err
	}
	s.RUnlock()
	log.Printf("%v - %v", s.author, s.quote)
	return &serv.GetQuoteResponse{Author: s.author, Quote: s.quote}, nil
}
