package grpc

import (
	"fmt"
	"google.golang.org/grpc"
	serv "gotest/servergrpc/pb"
	"log"
	"net"
	"os"
	"sync"
)

// ServerData grpc server data
type ServerData struct {
	serv.UnimplementedGetQuoteServer
	quote  string
	author string

	sync.RWMutex
}

// Run grpc server start
func (s *ServerData) Run(address string, port int) {
	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", address, port))

	log.Printf("listen grpc on addres - %s port - %d", address, port)

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	g := grpc.NewServer()
	serv.RegisterGetQuoteServer(g, &ServerData{})
	log.Printf("listen at %v", lis.Addr())
	if err := g.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
	log.Printf("GRPC started at ip - %s port %s", string(os.Getenv("SERVER_ADDRESS")), string(os.Getenv("PORTGRPC")))

}

// NewGrpcServer return new server
func NewGrpcServer() *ServerData {
	return &ServerData{serv.UnimplementedGetQuoteServer{}, "", "", sync.RWMutex{}}
}
