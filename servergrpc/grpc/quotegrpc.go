package grpc

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

// RandomquoteGrpc call quote request
func (s *ServerData) RandomquoteGrpc() (string, string, error) {

	type Quote struct {
		ID           string   `json:"_id"`
		Content      string   `json:"content"`
		Author       string   `json:"author"`
		Tags         []string `json:"tags"`
		AuthorSlug   string   `json:"authorSlug"`
		Length       int      `json:"length"`
		DateAdded    string   `json:"dateAdded"`
		DateModified string   `json:"dateModified"`
	}

	quoteChan := make(chan *http.Response)

	go asynqrequestgrpc("https://api.quotable.io/random", quoteChan) //todo move addr to env
	q := <-quoteChan
	bytes, _ := io.ReadAll(q.Body)

	var quote Quote
	if err := json.Unmarshal(bytes, &quote); err != nil { // Parse []byte to go struct pointer
		log.Fatalf("Can not unmarshal JSON")
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatalf(err.Error())
		}
	}(q.Body)

	return quote.Author, quote.Content, nil

}

// asynqrequestgrpc make grpc request
func asynqrequestgrpc(url string, rc chan *http.Response) {
	response, err := http.Get(url)
	if err != nil {
		log.Fatalf(err.Error())
	}
	rc <- response
}
