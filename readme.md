## Application Run
In order to following task I decided to add docker and make file, implement http with async request  and simple architecture and add grpc server and client

#1 run
`git clone`

#2 Run simple http server into docker
`docker up `

#3 make request to http server 
`curl http://127.0.0.14:27017/quote`


#4 Run grpc server
`meke upgrpcserver`

#5 Run grpc client
`meke upgrpcclient`
