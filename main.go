package main

import (
	"context"
	//"context"
	"github.com/joho/godotenv"
	"gotest/api"
	"log"
	"os"
	"os/signal"
	"syscall"
)

// main
func main() {
	_, cancel := context.WithCancel(context.Background())
	env, err := godotenv.Read(".env")
	if err != nil {
		log.Fatalf("Error loading .env file: %s", err)
	}

	rs := api.NewRESTServer()
	rh := api.NewHTTPHandler()
	go func() {
		if err = rs.Run(string(env["SERVER_ADDRESS"]), string(env["PORT"]), rh.InitRoutes()); err != nil {
			log.Fatalf("error starting HTTP server: %s", err.Error())
		}

	}()
	if err != nil {
		log.Fatalf("server error %s", err)
	}
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit
	cancel()
	log.Print("<===exiting app===>")

	log.Print("server stopped")

}

//func GetQuote(w http.ResponseWriter, req *http.Request) {
//
//	fmt.Fprintf(w, "hello\n")
//}
