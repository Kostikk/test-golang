FROM golang:1.22-alpine as builder

ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

WORKDIR /build

RUN apk update && apk upgrade && \
    apk --no-cache add ca-certificates && \
    apk --no-cache add tzdata

COPY go.mod .
COPY go.sum .
COPY .env .
RUN go mod download

COPY . .

RUN go build -o test main.go

WORKDIR /dist

RUN cp /build/test .

# Build a small image
FROM gcr.io/distroless/static

# copy files
COPY --from=builder /dist/test /
COPY .env .

ENTRYPOINT [ "/test" ]

EXPOSE 27017
