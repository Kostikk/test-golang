package api

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"net/http"
)

// Randomquote send request to server amd forms json responce
func (h *HTTPHandler) Randomquote(c *gin.Context) {

	type Quote struct {
		ID           string   `json:"_id"`
		Content      string   `json:"content"`
		Author       string   `json:"author"`
		Tags         []string `json:"tags"`
		AuthorSlug   string   `json:"authorSlug"`
		Length       int      `json:"length"`
		DateAdded    string   `json:"dateAdded"`
		DateModified string   `json:"dateModified"`
	}

	quoteChan := make(chan *http.Response)

	go asynqrequest(c, "https://api.quotable.io/random", quoteChan) //todo move addr to env
	q := <-quoteChan

	if q == nil {
		ErrorResponse(c, http.StatusInternalServerError, "error sending request")
		return
	}
	if q.StatusCode == http.StatusNotFound {
		ErrorResponse(c, http.StatusInternalServerError, "api request error")
		return
	}
	bytes, err := io.ReadAll(q.Body)
	if err != nil {
		ErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	var quote Quote
	if err := json.Unmarshal(bytes, &quote); err != nil { // Parse []byte to go struct pointer
		log.Fatalf("Can not unmarshal JSON")
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			ErrorResponse(c, http.StatusInternalServerError, err.Error())
			return
		}
	}(q.Body)
	SuccessResponse(c, http.StatusOK, gin.H{
		"author": quote.Author,
		"quote":  quote.Content,
	})

	//c.JSON(http.StatusOK, gin.H{
	//	"author": quote.Author,
	//	"quote":  quote.Content,
	//}) // cast it to string before showing

}

// asynqrequest
func asynqrequest(c *gin.Context, url string, rc chan *http.Response) {
	response, err := http.Get(url)
	if err != nil {
		rc <- nil
		return
	}
	rc <- response
	return
}
