package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// ReturnAPIErrorResponse data responce
type ReturnAPIErrorResponse struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// SuccessResponse return json Success Response
func SuccessResponse(c *gin.Context, status int, h gin.H) {

	c.JSON(http.StatusOK, h)
}

// ErrorResponse return json Error Response
func ErrorResponse(c *gin.Context, statusCode int, message string) {

	resp := ReturnAPIErrorResponse{
		Message: "error",
		Data:    message,
	}
	c.AbortWithStatusJSON(statusCode, resp)
}
