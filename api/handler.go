package api

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// HTTPHandler data
type HTTPHandler struct {
}

// NewHTTPHandler return new hamdler
func NewHTTPHandler() *HTTPHandler {
	return &HTTPHandler{}
}

// InitRoutes init routes for server
func (h *HTTPHandler) InitRoutes() *gin.Engine {
	router := gin.New()

	router.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "PONG\n")
	})

	router.GET("/quote", h.Randomquote)

	return router
}
