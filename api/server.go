package api

import (
	"context"
	"log"
	"net/http"
)

// Server server srtucture
type Server struct {
	httpServer *http.Server
}

// NewRESTServer New server
func NewRESTServer() *Server {
	return &Server{}
}

// Run runs server
func (s *Server) Run(ip string, port string, handler http.Handler) error {
	s.httpServer = &http.Server{
		Addr:    ip + ":" + port,
		Handler: handler,
	}
	log.Printf("restr server started at ip - %s port %s", ip, port)
	return s.httpServer.ListenAndServe()
}

// Shutdown stop server
func (s *Server) Shutdown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}
