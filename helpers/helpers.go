package helpers

import "github.com/gin-gonic/gin"

// SetUpRouter help for test
func SetUpRouter() *gin.Engine {
	router := gin.Default()
	return router
}
