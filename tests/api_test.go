package tests

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gotest/helpers"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRandomquote(t *testing.T) {
	mockResponse := `{"author":"Smart Guy","quote":"blabla"}`
	r := helpers.SetUpRouter()
	r.GET("/quote", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"quote":  "blabla",
			"author": "Smart Guy",
		})
	})
	req, _ := http.NewRequest("GET", "/quote", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := io.ReadAll(w.Body)
	assert.Equal(t, mockResponse, string(responseData), "ok")
	assert.Equal(t, http.StatusOK, w.Code, "ok")
}

func TestPingRoute(t *testing.T) {
	gin.SetMode(gin.TestMode)

	router := gin.Default()
	router.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/ping", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code, "dada")

	respBody, _ := io.ReadAll(w.Body)
	//assert.Nil(t, err)
	assert.Equal(t, `{"message":"pong"}`, string(respBody), "adad")
}
