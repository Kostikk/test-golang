package main

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	pb "gotest/client/clientpb/pb"
	"log"
	"strconv"
)

func main() {
	log.Printf("start client GRPC service")
	env, err := godotenv.Read(".env")
	if err != nil {
		log.Fatalf("Error loading .env file: %s", err)
	}
	addr := string(env["DIALADDR"])
	port, err := strconv.Atoi(env["DIALPORT"])
	if err != nil {
		log.Fatalf("Error %s", err)
	}
	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", addr, port), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("Error grpc dial %s", err)
	}
	log.Printf("dial addr %s port %d", addr, port)
	defer conn.Close()

	if err != nil {
		log.Fatalf("fail to connect %s", err)
	}
	// Code removed for brevity

	client := pb.NewGetQuoteClient(conn)

	// Note how we are calling the GetBookList method on the server
	// This is available to us through the auto-generated code
	data, err := client.GetQuote(context.Background(), &pb.GetQuoteRequest{})
	if err != nil {
		log.Fatalf("Error request %s", err)
	}
	log.Printf("AUTHOR - %s QUOTE - %v", data.Author, data.Quote)
}
